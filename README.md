# Test Server (server-in-container)

## Link - [server on heroku](https://server-in-container.herokuapp.com)

### API:
* [All Endpoints](https://server-in-container.herokuapp.com/api/endpoint)
* [Echo](https://server-in-container.herokuapp.com/api/echo)
* [DateTime](https://server-in-container.herokuapp.com/api/datetime)

#### Requirements:
* Docker
* Heroku
* Heroku plugin: heroku-container-tools
* Account on Heroku

#### How to?
1. Console: `npm init`
2. Console: `npm install express --save`
3. Create file: [app/server.js](app/server.js)
4. Create file: [Dockerfile](Dockerfile)
5. Console: `docker build -t <image-name>:<tag-name> .`
6. Console: `heroku login`
7. Console: `heroku create <app-name>`
8. Console: `heroku container:push web --app <app-name>`
9. Console: `heroku container:release web --app <app-name>`
10. Console: `heroku open --app <app-name>`